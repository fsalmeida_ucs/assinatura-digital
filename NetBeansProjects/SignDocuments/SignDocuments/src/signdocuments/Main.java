/*
 * The $organization License
 *
 * Copyright 2017 Fábio Santos Almeida <fabio.almeida at unicid.edu.br>.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */
package signdocuments;

/**
 * Universidade Cruzeiro do Sul© Copyright 2017.
 *
 * project displayname is SignDocuments Main.java, encoding is UTF-8.
 *
 * @author Fábio Santos Almeida <fabio.almeida at unicid.edu.br>
 * @version 1.0
 * @since 08/12/2017.
 *
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String fsalmeidaConfigName = "C:\\ucs\\fsalmeida.cfg"; //"C://ucs/keystore/fsalmeida.keystore";
        new SignDocuments().assinar(fsalmeidaConfigName);
        System.out.println("Documento assinado com sucesso?");
        // Caused by: java.io.FileNotFoundException: fsalmeida.cfg (O sistema não pode encontrar o arquivo especificado)
    }

}
