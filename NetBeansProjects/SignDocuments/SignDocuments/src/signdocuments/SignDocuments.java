/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package signdocuments;

import java.security.Provider;
import java.security.Security;

/**
 *
 * @author fsalmeida
 */
public class SignDocuments {

    public void assinar(String configName) {
        System.out.println("Assina digitalmente um documento.");
        registerProvider(configName);
    }

    private void registerProvider(String configName) {
        Provider p = new sun.security.pkcs11.SunPKCS11(configName);
        Security.addProvider(p);
    }

}
