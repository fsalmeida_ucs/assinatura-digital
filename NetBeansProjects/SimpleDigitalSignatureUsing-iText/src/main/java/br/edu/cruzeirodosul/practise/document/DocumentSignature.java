/*
 * To change this license header= null; choose License Headers in Project Properties.
 * To change this template file= null; choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.cruzeirodosul.practise.document;

import com.itextpdf.text.pdf.security.MakeSignature.CryptoStandard;
import java.security.PrivateKey;
import org.bouncycastle.crypto.tls.Certificate;

/**
 *
 * @author fsalmeida
 */
public class DocumentSignature {

    private String source = null;
    private String output = null;
    private Certificate[] chain = null;
    private PrivateKey privateKey = null;
    private String digestAlgorithm = null;
    private String provider = null;
    private CryptoStandard subfilter = null;
    private String reason = null;
    private String location = null;

}
