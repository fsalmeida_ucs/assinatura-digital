/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.cruzeirodosul.practise;

import com.itextpdf.text.DocumentException;
import com.itextpdf.text.pdf.security.DigestAlgorithms;
import com.itextpdf.text.pdf.security.MakeSignature;
import com.itextpdf.text.pdf.security.MakeSignature.CryptoStandard;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.Provider;
import java.security.Security;
import java.security.UnrecoverableKeyException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.Before;
import org.junit.Test;
import sun.security.pkcs11.SunPKCS11;

/**
 *
 * @author fsalmeida
 */
public class DigitalSignaturesForDocumentsTest {

    private String src = null; //OK!
    private String dest = null; //OK!
    private String digestAlgorithm = null; //OK!
    private String provider = null; //OK!
    private String reason = "Registro Acadêmico";
    private String location = "São Paulo";
    private Certificate[] chain = null; //OK!
    private PrivateKey privateKey = null; //OK!
    private MakeSignature.CryptoStandard subfilter = CryptoStandard.CMS;
    private KeyStore keyStore = null;
    private DigitalSignaturesForDocuments instance;

    @Before
    public void setup() {

        char[] password = "password".toCharArray();
        String prodeskConfigName = "C:\\\\Users\\\\fsalmeida\\\\prodesk.cfg";
        String safeNet5100 = "C:\\\\Users\\\\fsalmeida\\\\eToken.cfg";
        Provider providerPKCS11 = Security.getProvider("SunPKCS11");
        providerPKCS11 = providerPKCS11.configure(safeNet5100);
        Security.addProvider(providerPKCS11);

        src = "C://Users//fsalmeida//Documents//tarefa//2017.11.27.assinatura-digital//src.pdf";
        dest = "C://Users//FSALME~1//AppData//Local//Temp//dest.pdf";
        digestAlgorithm = DigestAlgorithms.SHA256;
        provider = providerPKCS11.getName();
        instance = new DigitalSignaturesForDocuments();

        try {
            keyStore = KeyStore.getInstance("PKCS11");
            keyStore.load(null, password);
            String alias = (String) keyStore.aliases().nextElement();
            chain = keyStore.getCertificateChain(alias);
            privateKey = (PrivateKey) keyStore.getKey(alias, password);
        } catch (KeyStoreException
                | IOException
                | NoSuchAlgorithmException
                | CertificateException
                | UnrecoverableKeyException ex) {
            Logger.getLogger(DigitalSignaturesForDocumentsTest.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Test
    public void assinaUmDocumentoQualquer() {
        try {
            instance.sign(
                    src,
                    dest,
                    chain,
                    privateKey,
                    digestAlgorithm,
                    provider,
                    subfilter,
                    reason,
                    location);
            System.out.println("Documento assinado digitalmente com sucesso!!!");

        } catch (GeneralSecurityException
                | IOException
                | DocumentException ex) {
            Logger.getLogger(DigitalSignaturesForDocumentsTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
