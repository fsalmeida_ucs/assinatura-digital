/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.berthou.signapdfdocument.practise;

import java.io.*;
import java.security.*;
import java.security.cert.Certificate;

import com.lowagie.text.*;
import com.lowagie.text.pdf.*;
import sun.security.pkcs11.SunPKCS11;

/**
 * Universidade Cruzeiro do Sul© Copyright 2017.
 *
 * project displayname is sign-a-pdf-document signPdfDocument.java, encoding is
 * UTF-8.
 *
 * @author Fábio Santos Almeida <fabio.almeida at unicid.edu.br>
 * @version 1.0
 * @since 11/12/2017.
 *
 */
public class signPdfDocument {

    /**
     * Nom du document PDF généré non signé
     */
    static String fname = "C:\\MonRep\\HelloWorld.pdf";

    /**
     * Nom du document PDF généré signé
     */
    static String fnameS = "C:\\MonRep\\HelloWorld_sign.pdf";

    public static void main(String[] args) {
        try {
            //signPdfDocument.buildPDF();
            signPdfDocument.signPdf();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Création d'un simple document PDF "Hello World"
     */
    public static void buildPDF() {

//        // Creation du document
//        Document document = new Document();
//
//        try {
//            // Creation du "writer" vers le doc
//            // directement vers un fichier
//            PdfWriter.getInstance(document,
//                    new FileOutputStream(fname));
//            // Ouverture du document
//            document.open();
//
//            // Ecriture des datas
//            document.add(new Paragraph("Hello World"));
//
//        } catch (DocumentException de) {
//            System.err.println(de.getMessage());
//        } catch (IOException ioe) {
//            System.err.println(ioe.getMessage());
//        }
//
//        // Fermeture du document
//        document.close();
    }

    /**
     * Signature du document
     */
    public static final boolean signPdf()
            throws IOException, DocumentException, Exception {
        // Vous devez preciser ici le chemin d'acces a votre clef pkcs12
        String fileKey = "C:\\MonRep\\pkcs12.cer";
        // et ici sa "passPhrase"
        String fileKeyPassword = "abacate";

        try {

            // Creation d'un KeyStore
            KeyStore ks = KeyStore.getInstance("pkcs12");

            // Chargement du certificat p12 dans el magasin
            ks.load(new FileInputStream(fileKey), fileKeyPassword.toCharArray());
            String alias = (String) ks.aliases().nextElement();

            // Recupération de la clef privée
            PrivateKey key = (PrivateKey) ks.getKey(alias, fileKeyPassword.toCharArray());

            // et de la chaine de certificats
            Certificate[] chain = ks.getCertificateChain(alias);

            // Lecture du document source
            PdfReader pdfReader = new PdfReader((new File(fname)).getAbsolutePath());
            File outputFile = new File(fnameS);

            // Creation du tampon de signature
            PdfStamper pdfStamper;
            pdfStamper = PdfStamper.createSignature(pdfReader, null, '\0', outputFile);
            PdfSignatureAppearance sap = pdfStamper.getSignatureAppearance();
            sap.setCrypto(key, chain, null, PdfSignatureAppearance.SELF_SIGNED);
            sap.setReason("Cruzeiro do Sul 2017. Todos os direitos reservados. Política de Privacidade.");
            sap.setLocation("");

            // Position du tampon sur la page (ici en bas a gauche page 1)
            sap.setVisibleSignature(new Rectangle(100, 100, 500, 300), 1, "sign_rbl");

            pdfStamper.setFormFlattening(true);
            pdfStamper.close();

            return true;
        } catch (Exception key) {
            throw new Exception(key);
        }
    }
}
