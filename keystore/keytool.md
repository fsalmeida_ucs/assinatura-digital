Gera um par de chaves

Op▒▒es:

 -alias <alias>                  nome do alias da entrada a ser processada
 -keyalg <keyalg>                nome do algoritmo da chave
 -keysize <keysize>              tamanho do bit da chave
 -sigalg <sigalg>                nome do algoritmo de assinatura
 -destalias <destalias>          alias de destino
 -dname <dname>                  nome distinto
 -startdate <startdate>          data/hora inicial de validade do certificado
 -ext <value>                    extens▒o X.509
 -validity <valDays>             n▒mero de dias da validade
 -keypass <arg>                  senha da chave
 -keystore <keystore>            nome da ▒rea de armazenamento de chaves
 -storepass <arg>                senha da ▒rea de armazenamento de chaves
 -storetype <storetype>          tipo de ▒rea de armazenamento de chaves
 -providername <providername>    nome do fornecedor
 -providerclass <providerclass>  nome da classe do fornecedor
 -providerarg <arg>              argumento do fornecedor
 -providerpath <pathlist>        classpath do fornecedor
 -v                              sa▒da detalhada
 -protected                      senha por meio de mecanismo protegido

Use "keytool -help" para todos os comandos dispon▒veis

Keystore: fsalmeida.keystore
senha...: password
```
keytool -genkeypair -alias demo -keyalg RSA -keysize 2048 -keystore fsalmeida.keystore
```


Keystore only has one password. You can change it using keytool:
```
keytool -storepasswd -keystore my.keystore
```

To change the key's password:
```
keytool -keypasswd  -alias <key_name> -keystore my.keystore
```



public class EncryptDecrypt {
protected KeyStore ks;
public EncryptDecrypt(String keystore, String ks_pass)
throws GeneralSecurityException, IOException {
initKeyStore(keystore, ks_pass);
}
public void initKeyStore(String keystore, String ks_pass)
throws GeneralSecurityException, IOException {
ks = KeyStore.getInstance(KeyStore.getDefaultType());
ks.load(new FileInputStream(keystore), ks_pass.toCharArray());
}
public X509Certificate getCertificate(String alias)
throws KeyStoreException {
return (X509Certificate) ks.getCertificate(alias);
}
public Key getPublicKey(String alias)
throws GeneralSecurityException, IOException {
return getCertificate(alias).getPublicKey();
}
public Key getPrivateKey(String alias, String pk_pass)
throws GeneralSecurityException, IOException {
return ks.getKey(alias, pk_pass.toCharArray());
}
public byte[] encrypt(Key key, String message)
throws GeneralSecurityException {
Cipher cipher = Cipher.getInstance("RSA");
cipher.init(Cipher.ENCRYPT_MODE, key);
byte[] cipherData = cipher.doFinal(message.getBytes());
return cipherData;
}
public String decrypt(Key key, byte[] message)
throws GeneralSecurityException {
Cipher cipher = Cipher.getInstance("RSA");
cipher.init(Cipher.DECRYPT_MODE, key);
byte[] cipherData = cipher.doFinal(message);
return new String(cipherData);
}
public static void main(String[] args)
throws GeneralSecurityException, IOException {
EncryptDecrypt app =
new EncryptDecrypt("src/main/resources/ks", "password");
Key publicKey = app.getPublicKey("demo");
Key privateKey = app.getPrivateKey("demo", "password");
System.out.println("Let's encrypt 'secret message' with a public key");
byte[] encrypted = app.encrypt(publicKey, "secret message");
System.out.println("Encrypted message: " + new BigInteger(1, encrypted).toString(16));
System.out.println("Let's decrypt it with the corresponding private key");
String decrypted = app.decrypt(privateKey, encrypted);
System.out.println(decrypted);
System.out.println("You can also encrypt the message with a private key");
encrypted = app.encrypt(privateKey, "secret message");
System.out.println("Encrypted message: " + new BigInteger(1, encrypted).toString(16));
System.out.println("Now you need the public key to decrypt it");
decrypted = app.decrypt(publicKey, encrypted);
System.out.println(decrypted);
}
}
