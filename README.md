
O que é uma Assinatura Digital?

Assinatura digital é uma assinatura eletrônica que usa um certificado digital para identificar o signatário. Conferindo às assinaturas digitais as seguintes características:

    Autenticidade: uma assinatura digital é inequivocamente ligada ao certificado digital do signatário.
    Integridade: cada assinatura digital é vinculada a um documento eletrônico, dessa forma qualquer alteração sofrida pelo documento eletrônico será perceptível pela assinatura digital.
    Não-repúdio: uma assinatura digital feita enquanto o certificado digital do signatário for válido não pode ter sua autoria negada pelo signatário.

No Portal de Assinaturas Certisign você assina seus documentos com certificados digitais ICP-Brasil.



O que significa Não-Repúdio?

O não repudio fornece provas de que um usuário realizou uma determinada ação, seu autor não poderá, por forças tecnológicas e legais, negar que seja o responsável por seu conteúdo.

A tecnologia da Assinatura Digital da ICP-Brasil prevê essa capacidade.



O que é PAdES?

PDF Advanced Electronic Signature (PAdES) é um padrão internacional de assinatura digital exclusivo para arquivos PDF. Esse padrão é um dos padrões de assinaturas digitais aceitos pela ICP-Brasil

PAdES - Arquivos no formato PDF, sem protocolo

O padrão PAdES é recomendado para assinatura de documentos no formato PDF.

Um dos principais diferenciais do PAdES é a representação visual da assinatura no próprio documento, permitindo o uso de imagens e outras informações do assinante - assemelhando-se à uma assinatura manual, colaborando com a sua aceitação.




O que é CAdES?

CMS Advanced Electronic Signature (CAdES) é o padrão internacional mais difundido de todos. Esse padrão faz parte dos padrões de assinaturas digitais aceitos pela ICP-Brasil.

Uma das características de sua grande aceitação é a capacidade de assinar qualquer tipo de arquivo digital. No entanto, esse padrão de assinatura produz um arquivo p7s, que não é de fácil visualização, por isso o Portal de Assinaturas produz um arquivo de manifesto dos documentos assinados por esse padrão.

O manifesto de um documento assinado é um documento de rastreabilidade das assinaturas realizadas e do documento original. Esse manifesto contém códigos de verificação, código de barra e QRCode para que, mesmo impresso, você consiga acessar o documento original assinado.




# Assinatura Digital e Verificação de Assinaturas #

 CertiSigner

Evolução do produto SDK (Kit de Desenvolvimento de Software), o CertiSigner é um framework de desenvolvimento que permite integrar a Certificação Digital em suas aplicações, tudo de forma ágil e fácil.

Pode ser integrado a sistemas novos ou já existentes, como:

* Workflow (fluxo de trabalho);
* Gerenciador de documentos (GED);
* Emissor de Nota Fiscal Eletrônica.





[sun.security.pkcs11.SunPKCS11; -- package does no exist exist](http://javac.com.br/jc/posts/list/18/46.page)
sunpkcs11.jar
questão
    Reinstalei o JDK e não apareceu nada de SunPKCS11.
    Dai eu baixeu o JDK para linux, por ser um arquivo compactado, extrai e peguei de lá mesmo o SunPKCS11.jar, no diretório /jre/lib/ext, adicionei no classpath da aplicação e o erro sumiu.
    Segundo esses links, era para esse .jar estar presente:
    http://www.oracle.com/technetwork/java/javase/jdk-7-readme-429198.html
    http://www.oracle.com/technetwork/java/javase/jre-7-readme-430162.html
    Agora é só conseguir um token/cartão para fazer o teste.
    Talvez esse arquivo SunPKCS11.jar venha em outras instalações do JDK para Windows, porém aqui no JDK 7 update 10 de 64 Bits esse arquivo não foi encontrado, vi relatos de pessoas com esse problema também em fóruns internacionais.
    Não sei se ajuda, mas vou deixar aqui o SunPKCS11.jar, talvez alguém precise.
    Obrigado. 

[Java PKCS#11 Reference Guide](https://docs.oracle.com/javase/7/docs/technotes/guides/security/p11guide.html)
    1.0 Introduction

    The Java platform defines a set of programming interfaces for performing cryptographic operations. These interfaces are collectively known as the Java Cryptography Architecture (JCA) and the Java Cryptography Extension (JCE). Specifications are available at the Java SE Security Documentation page.

    The cryptographic interfaces are provider-based. Specifically, applications talk to Application Programming Interfaces (APIs), and the actual cryptographic operations are performed in configured providers which adhere to a set of Service Provider Interfaces (SPIs). This architecture supports different provider implementations. Some providers may perform cryptographic operations in software; others may perform the operations on a hardware token (for example, on a smartcard device or on a hardware cryptographic accelerator).

    The Cryptographic Token Interface Standard, PKCS#11, is produced by RSA Security and defines native programming interfaces to cryptographic tokens, such as hardware cryptographic accelerators and Smartcards. To facilitate the integration of native PKCS#11 tokens into the Java platform, a new cryptographic provider, the Sun PKCS#11 provider, has been introduced into the J2SE 5.0 release. This new provider enables existing applications written to the JCA and JCE APIs to access native PKCS#11 tokens. No modifications to the application are required. The only requirement is the proper configuration of the provider into the Java Runtime.

    Although an application can make use of most PKCS#11 features using existing APIs, some applications might need more flexibility and capabilities. For example, an application might want to deal with Smartcards being removed and inserted dynamically more easily. Or, a PKCS#11 token might require authentication for some non-key-related operations and therefore, the application must be able to log into the token without using keystore. In J2SE 5.0, the JCA was enhanced to allow applications greater flexibility in dealing with different providers.

    This document describes how native PKCS#11 tokens can be configured into the Java platform for use by Java applications. It also describes the enhancements that were made to the JCA to make it easier for applications to deal with different types of providers, including PKCS#11 providers.

[JDK 8 PKCS#11 Reference Guide](https://docs.oracle.com/javase/8/docs/technotes/guides/security/p11guide.html#Config)

[Access restriction on sun.security.pkcs11.SunPKCS11](https://stackoverflow.com/questions/5172069/access-restriction-on-sun-security-pkcs11-sunpkcs11)

question
    I'm trying to setup a PKCS11 provider for accessing a smartcard. I installed a PKCS11 library on my system and followed the instructions in the Java PKCS#11 Reference Guide. In the reference they simply create an instance of sun.security.pkcs11.SunPKCS11 and pass the name of the configuration file to the constructor. When I try to compile the following code

    Provider p = new sun.security.pkcs11.SunPKCS11("pkcs11.cfg");
    Security.addProvider(p);

    I get the following error.

        Access restriction: The constructor SunPKCS11(String) is not accessible due to restriction on required library /usr/lib/jvm/java-6-sun-1.6.0.24/jre/lib/ext/sunpkcs11.jar

    What am I doing wrong? I use Eclipse 3.5 with Java SE 1.6 under Ubuntu x86.

    Best regards.

answers
    Look into the projects's properties and open the Libraries tab. I assume you have set the JRE System Library to an execution environment. Change it to the workspace JRE or select a specific JRE manually.

    Background: By selecting an execution environment you say that you want to write an app that is compliant to the Java API. The class sun.security.pkcs11.SunPKCS11 is located in the sun package which marks it as proprietary to Sun Java implementation and is not part of the standard Java API.


[Java Cryptography Architecture (JCA) Reference Guide](https://docs.oracle.com/javase/8/docs/technotes/guides/security/crypto/CryptoSpec.html)
Introduction

The Java platform strongly emphasizes security, including language safety, cryptography, public key infrastructure, authentication, secure communication, and access control.

The JCA is a major piece of the platform, and contains a "provider" architecture and a set of APIs for digital signatures, message digests (hashes), certificates and certificate validation, encryption (symmetric/asymmetric block/stream ciphers), key generation and management, and secure random number generation, to name a few. These APIs allow developers to easily integrate security into their application code. The architecture was designed around the following principles:

    Implementation independence: Applications do not need to implement security algorithms. Rather, they can request security services from the Java platform. Security services are implemented in providers (see below), which are plugged into the Java platform via a standard interface. An application may rely on multiple independent providers for security functionality.

    Implementation interoperability: Providers are interoperable across applications. Specifically, an application is not bound to a specific provider, and a provider is not bound to a specific application.

    Algorithm extensibility: The Java platform includes a number of built-in providers that implement a basic set of security services that are widely used today. However, some applications may rely on emerging standards not yet implemented, or on proprietary services. The Java platform supports the installation of custom providers that implement such services.

Other cryptographic communication libraries available in the JDK use the JCA provider architecture, but are described elsewhere. The Java Secure Socket Extension (JSSE) provides access to Secure Socket Layer (SSL) and Transport Layer Security (TLS) implementations. The Java Generic Security Services (JGSS) (via Kerberos) APIs, and the Simple Authentication and Security Layer (SASL) can also be used for securely exchanging messages between communicating applications.



[ Certisign > Atendimento e Suporte > Sistemas Homologados ](https://www.certisign.com.br/sistemas)

    Sistemas homologados para a emissão e utilização do Certificado Digital em Windows 10 - 64 bits.

[CodeSigning for Java](https://www.certisign.com.br/certificado-digital/assinatura-codigo/java#aba)
Proteja conteúdos e softwares desenvolvidos para a plataforma Java. Assine objetos e demais produtos do desenvolvimento de software para integridade dos dados e garantia de procedência

[Central de Atendimento e Suporte Certisign](https://www.certisign.com.br/atendimento-suporte)
Veja os sistemas homologados para a emissão e utilização do Certificado Digital

[dll para o cartão SafeWeb serasa [RESOLVIDO] ](http://www.javac.com.br/jc/posts/list/193-dll-para-o-cartao-safeweb-serasa-resolvido.page)

[aetpkss1.dll, 1 versão disponível](https://pt.dll-files.com/aetpkss1.dll.html)
erros de DLL permanentemente. Se você não sabe exatamente como instalar o DLL ou não tem certeza de que versão escolher, esse é o caminho.

[Como instalar o SafeSign 64-bits no Windows 10 build 10586](https://www.baboo.com.br/windows/windows-10/como-instalar-o-safesign-64-bits-no-windows-10-build-10586/)


### SIAA - Certificação de Arquivo ###
```
Username: jssantos
Password: 39059972821

jssantos - 39059972821
```