/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.edu.cruzeirodosul.practise;

import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.UnrecoverableKeyException;
import java.security.cert.X509Certificate;
import org.demoiselle.signer.core.CertificateLoader;
import org.demoiselle.signer.core.CertificateLoaderImpl;
import org.demoiselle.signer.core.exception.CertificateCoreException;
import org.demoiselle.signer.core.keystore.loader.factory.KeyStoreLoaderFactory;
import org.demoiselle.signer.cryptography.AsymmetricAlgorithmEnum;
import org.demoiselle.signer.cryptography.Cryptography;
import org.demoiselle.signer.cryptography.factory.CryptographyFactory;

/**
 * Universidade Cruzeiro do Sul© Copyright 2017.
 *
 * KeyStoreCertificadoA3.java
 *
 * @author Fábio Santos Almeida <fabio.almeida at unicid.edu.br>
 * @version 1.0
 * @since 28/11/2017.
 *
 */
public class KeyStoreCertificadoA3 {

    KeyStore keyStore = null;
    PrivateKey privateKey = null;

    public static void main(String[] args) {

        /* Senha do dispositivo */
        String PIN = "acl3264";
        //String PIN = "senha_do_token";

        new KeyStoreCertificadoA3().executa(PIN);
    }

    private Cryptography setupCriptography() {
        /*Configurando o Criptography */
        Cryptography crypto = CryptographyFactory.getInstance().factoryDefault();
        crypto.setAlgorithm(AsymmetricAlgorithmEnum.RSA);
        crypto.setProvider(keyStore.getProvider());
        return crypto;
    }

    private PublicKey obterChavePublica(String PIN) throws CertificateCoreException {
        /* Obtendo a chave publica */
        CertificateLoader cl = new CertificateLoaderImpl();
        X509Certificate x509 = cl.loadFromToken(PIN);
        PublicKey publicKey = x509.getPublicKey();
        return publicKey;
    }

    private void obterChavePrivada(String PIN) throws UnrecoverableKeyException, KeyStoreException, NoSuchAlgorithmException {
        /* Obtendo a chave privada */
        keyStore = KeyStoreLoaderFactory.factoryKeyStoreLoader().getKeyStore();
        //KeyStore keyStore = KeyStoreLoaderFactory.factoryKeyStoreLoader().getKeyStore(PIN);

        String alias = (String) keyStore.aliases().nextElement();
        privateKey = (PrivateKey) keyStore.getKey(alias, PIN.toCharArray());
    }

    private void executa(String PIN) {

        try {
            obterChavePrivada(PIN);

            PublicKey publicKey = obterChavePublica(PIN);

            Cryptography crypto = setupCriptography();

            /* criptografando com a chave privada */
            crypto.setKey(privateKey);
            byte[] conteudoCriptografado = crypto.cipher("SERPRO".getBytes());
            System.out.println(conteudoCriptografado);

            /* descriptografando com a chave publica */
            crypto.setKey(publicKey);
            byte[] conteudoAberto = crypto.decipher(conteudoCriptografado);
            System.out.println(new String(conteudoAberto));

        } catch (UnrecoverableKeyException e) {
            e.printStackTrace();
        } catch (KeyStoreException e) {
            e.printStackTrace();
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }

    }

}
