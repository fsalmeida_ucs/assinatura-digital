
--- 
### 2017-12-11 ###
http://www.flatmtn.com/article/creating-pkcs12-certificates

http://www.berthou.com/us/2007/11/28/sign-a-pdf-document/
Novo projeto: com.berthou.sign-a-pdf-document - cd C:\Users\fsalmeida\Documents\tarefa\2017.11.27.assinatura-digital\NetBeansProjects\sign-a-pdf-document;


--- 
### 2017-12-11 ###


--- 
### 2017-12-07 ###
*   [KeyStore Explorer is a free GUI replacement for the Java command-line utilities keytool and jarsigner.](http://www.keystore-explorer.org/)
*   [MultiPdfSigner - Sign multiple PDF files using rules, based on pages count](https://sourceforge.net/p/multipdfsigner/code/ref/master/)
*    DigitalSign_Ipad - Digital Sign App allows to create a digital sign on pdf
*    MultiPdfSigner - Sign multiple PDF files using rules, based on pages count
*   

--- 
### branch jfassis-revisao_rematricula-13.11.2017 ###
1. CruzeiroDoSul_Model
1. Academico_service
1. SIAA_ACADEMICO

```
keytool -list -keystore fsalmeida.keystore -storetype PKCS11 -providerclass sun.security.pkcs11.SunPKCS11 -providerArg eToken.cfg
jarsigner -verbose -tsa http://timestamp.digicert.com -keystore NONE -storetype PKCS11 -providerClass sun.security.pkcs11.SunPKCS11  -providerArg eToken.cfg  “/path/to/file.jar” “YOUR-CERTIFICATE-NAME”

/c/bin/java/jdk-9.0.1/bin/keytool -list -keystore NONE -storetype PKCS11 -providerclass sun.security.pkcs11.SunPKCS11 -providerArg /c/bin/java/jdk-9.0.1/conf/pkcs11.cfg
```


--- 
### Table of contents: ###

*  Understanding digital signatures
*  PDF and digital signatures
*  Certificate authorities, certificate revocation and time stamping
*  Options for signing documents externally
*  Validation of signed documents


--- 
### References ###

*   [Digital Signatures for PDF Documents](https://pages.itextpdf.com/ebook-digital-signatures-for-pdf.html)
*   [NF-e: Assinatura dos XMLs de Envio de Lote, Cancelamento e Inutilização - Certificado A3](http://javahome.com.br/jc/posts/list/18/122.page)
*   [GUI tool for administration of PKCS#11 enabled devices](https://sourceforge.net/projects/pkcs11admin/?source=typ_redirect)
*   [5 PKCS#11 Reference Guide](https://docs.oracle.com/javase/9/security/pkcs11-reference-guide1.htm#JSSEC-GUID-30E98B63-4910-40A1-A6DD-663EAF466991)
SunPKCS11 Provider
*   [Java security technology](https://docs.oracle.com/javase/8/docs/technotes/guides/security/index.html)
Java security technology includes a large set of APIs, tools, and implementations of commonly-used security algorithms, mechanisms, and protocols. The Java security APIs span a wide range of areas, including cryptography, public key infrastructure, secure communication, authentication, and access control. Java security technology provides the developer with a comprehensive security framework for writing applications, and also provides the user or administrator with a a set of tools to securely manage applications.
*   [JDK 8 PKCS#11 Reference Guide](https://docs.oracle.com/javase/8/docs/technotes/guides/security/p11guide.html#Config)
*   [SmartCard - PKCS11 - GemSAFE - Problema na leitura de certificado](http://www.guj.com.br/t/smartcard-pkcs11-gemsafe-problema-na-leitura-de-certificado/111460)
*   [Signing Java .jar Files with a Hardware Token in Linux](http://twalcari.github.io/blog/signing-java-jar-files-with-a-hardware-token-linux/)